//
//  MasterViewController.swift
//  SocialContentCollection
//
//  Created by Rachel Bodnar on 1/3/16.
//  Copyright © 2016 Rachel Bodnar. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var entries: [NSDictionary]?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let file = NSBundle.mainBundle().pathForResource("Sources", ofType: "plist") {
            entries = NSArray(contentsOfFile: file) as? [NSDictionary]
        }

        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
            
                if let entry = entries![indexPath.row] as? Dictionary<String, String> {
                    
                    let urlString = entry["URL"]
                    let url = NSURL(string: urlString!)
                    
                    controller.url = url
                }
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (entries?.count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        if let entry = entries![indexPath.row] as? Dictionary<String, String> {
            
            cell.textLabel?.text = entry["Title"]
            
        }

        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

}

