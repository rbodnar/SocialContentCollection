//
//  DetailViewController.swift
//  SocialContentCollection
//
//  Created by Rachel Bodnar on 1/3/16.
//  Copyright © 2016 Rachel Bodnar. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    var url: NSURL?
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let url = url {
            
            let urlRequest = NSURLRequest(URL: url)
            
            webView.loadRequest(urlRequest)
            
        }
        
        
    }
    
    
}